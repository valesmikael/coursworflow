// charger les modules

var gulp = require("gulp");
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var csscomb = require('gulp-csscomb');
var minify = require('gulp-minifier');

//tache gulp

gulp.task('prod', function() {
    return gulp.src('./projet/dev/**')
    .pipe(minify({
        minify: true,
        minifyHTML: {
            collapseWhitespace: true,
            conservativeCollapse: true,
        },
        minifyJS: true,
        minifyCSS: true
    }))
    .pipe(gulp.dest('./dist'));

});

gulp.task('comb', function() {
    return gulp.src('./projet/scss/**.scss')
      .pipe(csscomb())
      .pipe(gulp.dest('./projet/scss'));
  });

gulp.task('sass', function() {

    return gulp.src("./projet/scss/styles.scss")
        .pipe(sourcemaps.init())//on charge le sourcemaps
        .pipe(sass().on("error", sass.logError))
        .pipe(sourcemaps.write())// on ecrit le sourcemaps
        .pipe(gulp.dest("./projet/dev"));
        
});

gulp.task("serve", function(){

    browserSync.init({
        server: {
            baseDir: "./projet/dev"
        }
    }); 
    //Ecriture de browserSync
    gulp.watch("./projet/dev/**.**").on("change", browserSync.reload);

    //Ecriture standard pour regarder des fichier et lancer des taches
    gulp.watch("./projet/scss/**.**", ['sass']);

});
// ** initialisation pour nouveau projets*/
var fs = require("fs"); //systeme de fichier natif de nodejs

gulp.task("init", function(){

    var folders = [
        "./dist",
        "./projet",
        "./projet/dev",
        "./projet/scss"     
    ];

    var files = [
        "./projet/dev/index.html",
        "./projet/dev/script.js",
        "./projet/scss/styles.scss",
        ".csscomb.json"
    ];

    for (let folder of folders){

        if ( !fs.existsSync(folder)){
            fs.mkdirSync(folder);
        }
    }
    
    for (let file of files){

        if ( !fs.existsSync(file)){
            fs.writeFileSync(file);
        }
    }

});